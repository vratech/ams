package com.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@EnableJpaRepositories(entityManagerFactoryRef = "attendanceManagementEntityManager", transactionManagerRef = "attendanceManagementTransactionManager", basePackages = "com.attendanceManagement.repo")
public class AttendanceManagementConfig {
    @Autowired
    private Environment env;
    public AttendanceManagementConfig() {
        super();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean attendanceManagementEntityManager() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(attendanceManagementDataSource());
        em.setPackagesToScan(new String[]{"com.attendanceManagement.model"});

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        HashMap<String,Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto",env.getProperty("hibernate.hbm2ddl.auto"));
        properties.put("hibernate.dialect",env.getProperty("hibernate.dialect"));
        em.setJpaPropertyMap(properties);
        return em;
    }

    @Bean
    public DataSource attendanceManagementDataSource(){
        final HikariConfig config = new HikariConfig();
        config.setJdbcUrl(env.getProperty("app.attendanceManagement.datasource.url"));
        config.setUsername(env.getProperty("app.attendanceManagement.username"));
        config.setPassword(env.getProperty("app.attendanceManagement.password"));
        config.addDataSourceProperty("cachePrepStmts",env.getProperty("spring.datasource.master.cache-prepared-statements"));
        config.addDataSourceProperty("prepStmtCacheSize",env.getProperty("spring.datasource.master.cache-size-prepared-statements"));
        config.addDataSourceProperty("prepStmtCacheSqlLimit",env.getProperty("spring.datasource.master.cache-sql-limit-prepared-statements"));

        final HikariDataSource ds = new HikariDataSource(config);
        ds.setMaximumPoolSize(Integer.parseInt(env.getProperty("spring.datasource.master.maximumPoolSize")));
        ds.setConnectionTimeout(Integer.parseInt(env.getProperty("spring.datasource.master.connectionTimeout")));
        ds.setMinimumIdle(Integer.parseInt(env.getProperty("spring.datasource.master.minimumIdle")));
        ds.setMaxLifetime(Integer.parseInt(env.getProperty("spring.datasource.master.maxLifetime")));
        ds.setIdleTimeout(Integer.parseInt(env.getProperty("spring.datasource.master.idleTimeout")));
        ds.setPoolName(env.getProperty("spring.datasource.poolName"));

        ds.setDriverClassName(env.getProperty("app.attendaceManagement.datasource.driver-class-name"));
        return ds;
    }

    @Bean
    public PlatformTransactionManager attendanceManagementTransactionManager(){
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(attendanceManagementEntityManager().getObject());
        return transactionManager;
    }
}
