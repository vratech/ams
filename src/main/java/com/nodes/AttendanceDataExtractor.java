package com.nodes;

import lombok.extern.slf4j.Slf4j;
import orchestrator.IOrchestratorContext;
import orchestrator.WorkNode;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AttendanceDataExtractor extends WorkNode {

    @Override
    public boolean execute(IOrchestratorContext context){
        System.out.println("Hello in attendance node");
        return true;
    }
}
