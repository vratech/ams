package com.attendanceManagement.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="attendance_management")
//@NamedQuery(name="AttendanceManagement.findAll", query="SELECT am FROM AttendanceManagement am")
public class AttendanceManagement implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "uniqueId")
    private int uniqueId;

    @Column(name = "username")
    private String username;

    @Column(name = "inTime")
    private Date inTime;

    @Column(name = "outTime")
    private Date outTime;

    @Column(name = "courseId")
    private String courseId;

    @Column(name = "userId")
    private String userId;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getInTime() {
        return inTime;
    }

    public void setInTime(Date inTime) {
        this.inTime = inTime;
    }

    public Date getOutTime() {
        return outTime;
    }

    public void setOutTime(Date outTime) {
        this.outTime = outTime;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
