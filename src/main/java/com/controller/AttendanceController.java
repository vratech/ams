package com.controller;

import com.service.AttendanceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@CrossOrigin
@Api(value = "attendance_management", description = "Attendance Management Service")
@RestController
@Slf4j
@RequestMapping(value="/home")
public class AttendanceController implements IController{
    @Autowired
    private AttendanceService attendanceService;

    @Override
    public void doProcessing(){
//       log.info("do processing");
    }
    @ApiOperation(value = "Mark the attendance")
    @RequestMapping(value = "/attendance/markattendance", method = RequestMethod.GET)
    public void attendance() {
        attendanceService.getAttendanceService().doActivities();
    }

}
