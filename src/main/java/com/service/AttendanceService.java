package com.service;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import orchestrator.OrchestratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.nodes.*;

import javax.annotation.PostConstruct;

@Slf4j
@Service
public class AttendanceService{
    @Getter
    private OrchestratorService attendanceService;
    @Autowired
    private AttendanceDataExtractor attendanceDataExtractor;
    @PostConstruct
    public void init(){
        log.info("Attendance Service");
        attendanceService = new OrchestratorService();
        loadAttendance();
    }
    public OrchestratorService getAttendanceService(){ return  attendanceService;}
    public void loadAttendance(){
        attendanceService.addExecutorNode(attendanceDataExtractor);
    }
}